# Table of Contents: <!-- omit in toc -->

- [Unreleased Changes:](#unreleased-changes)
- [Released Changes:](#released-changes)
  - [OSA 1.16.5 Version RELEASE R-1.2.1](#osa-1165-version-release-r-121)
  - [OSA 1.16.5 Version ALPHA A-0.21.0](#osa-1165-version-alpha-a-0210)



# Unreleased Changes:

## OSA 1.16.5 Version BETA B-0.x.0

>### **Description**
>
>
>
>#### Updated Mods
>
>>- 
>#### Removed Mods
>
>>- 
>
>#### Added Mods
>
>> - 
>
>#### Configs
>
>> - 
>
>#### Quests
>
>> - 
>
>#### Scripts
>
>> - 
>
>#### Notes
>
>> - 
>
>



# Released Changes:

## OSA 1.16.5 Version RELEASE R-1.2.1

> ### Description
>
> -
>
> 
>
> #### Scripts
>
> >-

## OSA 1.16.5 Version BETA B-0.78.0

>### **Description**
>
>Updated mods, added Project Integration mod to make more emc items available
>
>Added a script to make crafting rubber easier
>
>#### Updated Mods
>
>>- CC: tweaked
>>- CoFH Core
>>- Collective (forge)
>>- Crafttweaker
>>- Extreme Reactors
>>- Feywild
>>- GeckoLib
>>- Macaw's Bridges
>>- Thermal Foundation
>>- ZeroCore 2
>
>#### Removed Mods
>
>>- 
>
>#### Added Mods
>
>> - Projecte integration
>
>#### Configs
>
>> - 
>
>#### Quests
>
>> - 
>
>#### Scripts
>
>> - fixes.zs
>>   - added recipe using freshwater to make rubber, this will make things easier
>
>#### Notes
>
>> - 
>
>

## OSA 1.16.5 Version BETA B-0.75.0

>### **Description**
>
>Small update adding a new compacting rule, some mods and a change to a projecte recipe
>
>also removed a mod
>
>#### Updated Mods
>
>>- 
>
>#### Removed Mods
>
>>- Illuminations FORGE
>
>#### Added Mods
>
>> - pipez
>> - bad wither no cookie reloaded
>
>#### Configs
>
>> - storage drawers
>>   - compacting rules
>>     - netherite debris nuggets - netherite scrap
>
>#### Quests
>
>> - 
>
>#### Scripts
>
>> - projecte
>>   - condenser - make philosophers stone not get consumed to follow in line with other recipes from projecte
>
>#### Notes
>
>> - 
>

## OSA 1.16.5 Version BETA B-0.74.1

>### **Description**
>
>small update making the mystical agriculture pedestal easier to craft
>
>#### Updated Mods
>
>>- 
>
>#### Removed Mods
>
>>- 
>
>#### Added Mods
>
>> - 
>
>#### Configs
>
>> - 
>
>#### Quests
>
>> - 
>
>#### Scripts
>
>> - mysticalagriculture
>>   - changed recipe of pedestal to be easier
>
>#### Notes
>
>> - 
>
>

## OSA 1.16.5 Version BETA B-0.74.0

>### **Description**
>
>Added mods AE Additions, AE2 Additional opportunity'
>
>Disabled Extra Botany Ego fight Disarm
>
>Added QoL conversions to the compacting drawer
>
>Updated scripts, fixed wired modem and futura block conflict, added crushed uranium to yellorite processing, added recipe to craft netherite debri
>
>#### Updated Mods
>
>>- 
>
>#### Removed Mods
>
>>- 
>
>#### Added Mods
>
>> - AE Additions
>> - AE2 Additional Opportunity
>
>#### Configs
>
>> - extra botany
>>   - disable egos disarm, to allow for modded weapons/armour to help the fight
>> - storage drawers
>>   - added compating rules:
>>   - stone - stone bricks
>>   - clay - clay block
>>   - quartz block - quartz
>>   - wool - string
>>   - snow block - snow balls
>>   - melon - melon slice
>>   - glowstone - glowstone dust
>
>#### Quests
>
>> - 
>
>#### Scripts
>
>> - fixes.zs
>>   - changed recipe of wired modem to fix conflict with futura block
>>   - add crushed uranium smelted into yellorite ingot
>>   - added netherite debris to scrap
>> - create
>>   - wash crushed uranium to yellorite
>
>#### Notes
>
>> - 
>
>

## OSA 1.16.5 Version BETA B-0.70.0

>### **Description**
>
>
>
>#### Updated Mods
>
>>- Better Spawner Control
>>- Collective
>>- Journeymap
>>- Macaws Bridges
>
>#### Removed Mods
>
>>- Custom Starter Gear
>
>#### Added Mods
>
>> - My Server is Compatible
>
>#### Configs
>
>> - Illuminations
>>   - disabled eyes due to a crash
>
>#### Quests
>
>> - Added starting gear quest to main questline
>
>#### Scripts
>
>> - miniutilities.zs
>>   - added recipe for ender storage block to 9 pearls
>
>#### Notes
>
>> - 
>
>

## OSA 1.16.5 Version BETA B-0.67.1

>### **Description**
>
>
>
>#### Updated Mods
>
>>- 
>
>#### Removed Mods
>
>>- 
>
>#### Added Mods
>
>> - 
>
>#### Configs
>
>> - Vein miner
>>   - removed granite, andesite, diorite from the stone block group 
>> - Akashic Tome
>>   - Added tinkers Materials and You, and Cooking for Blockheads I
>> - Advanced finders
>>   - added draconium ore to mineral finder
>>   - made mineral finder detect down to 1 ore (for draconium)
>
>#### Quests
>
>> - rearranged rewards for baubley heart canisters
>> - fixed typo in a main quest
>
>#### Scripts
>
>> - miniutilities.zs
>>   - changed recipe of healing axe to be end-game
>> - tags.zs
>>   - added tags for crafting andesite to some stones
>>   - added tags to all wings potions
>>   - added fey spring planks to oredict
>>   - added chisel marble/limestone to stone tag
>> - custom.zs
>>   - added new recipe for andesite
>> - wings.zs
>>   - changed stage to use tag for performance boost and simplification of reading
>> - advancedfinders.zs
>>   - changed recipes of finders to use repeaters instead of comparators
>
>#### Notes
>
>> - 

## OSA 1.16.5 Version BETA B-0.62.1

>### **Description**
>
>
>
>#### Updated Mods
>
>>- 
>
>#### Removed Mods
>
>>- 
>
>#### Added Mods
>
>-  
>
>#### Configs
>
>-  Large Ore Deposits
>   -  Reduced size and frequency of veins
>-  Spice Of Life
>   -  spread health out a bit more, kept same number of hearts
>-  Eyes in the darkness
>   -  disabled jumpscares
>-  Everlasting abilities
>   -  increased xp requirement to use them
>
>
>#### Quests
>
>> - 
>
>#### Scripts
>
>> - - 
>
>#### Notes
>
>> - 

## OSA 1.16.5 Version ALPHA A-0.62.0

>### **Description**
>
>
>
>#### Updated Mods
>
>>- Collective
>
>#### Removed Mods
>
>>- 
>
>#### Added Mods
>
>-  Advanced Finders
>
>#### Configs
>
>-  Large Ore Deposits
>   -  adjusted all deposits
>
>-  Advanced Finders
>   -  increased durability
>
>
>
>#### Quests
>
>> - Main Quests 
>>   - Extras
>>     - added quark quests
>> - Cooking
>>   - initialised chapter
>>   - getting started
>>     - quests to build a multiblock kitchen
>>   - delights
>>     - quests to cook some nethers/farmers delights foods
>>   - pams
>>     - quests to build some pams foods/foods extended recipes
>> - ComputerCraft
>>   - initialised chapter
>
>
>
>#### Scripts
>
>> - elevators.zs
>>   - changed elevator recipes to use mana pearls
>
>#### Notes
>
>> - 

## OSA 1.16.5 Version ALPHA A-0.53.0

>### **Description**
>
>
>
>#### Updated Mods
>
>>- 
>
>#### Removed Mods
>
>>- 
>
>#### Added Mods
>
>-  Wings
>
>#### Configs
>
>-  
>
>
>#### Quests
>
>> - Thermal Series
>>   - getting started
>>     - finished adding quests and rewards
>>   - cultivation
>>     - finished adding quests and rewards
>> - Main quests
>>   - started filling in quests
>> - Cardboard Boxes
>> - ChickenChunks
>>   - quest and reward for chunkloaders
>> - Ender Chests
>> - Ender Tanks
>> - Entangled
>
>#### Scripts
>
>> - extremereactors.zs
>>   - changed recipes of reactor and turbine casings, removed direct craft for hardened casings
>> - draconicevolution.zs
>>   - changed recipe of basic fusion injector to use alfsteel nuggets, enderium block and conjuration catalyst
>> - projecte.zs
>>   - changed recipe of philosophers stone to be end-game botania, mid game draconic
>>   - made energy condenser require a philosophers stone
>> - miniutilities.zs
>>   - disabled recipes of solar and lunar panels, and solar panel controller
>>   - added itemstages for angelrings
>> - compactmachines.zs
>>   - changed recipes of machine walls and personal shrinking device
>> - itemcollectors.zs
>>   - adjusted recipe of the collectors to use botania ingredients
>> - mysticalagriculture.zs
>>   - adjusted altar recipes to use wyvern cores and sin runes to push Mystical Agriculture to late-game
>> - wings.zs
>>   - item stages for potions
>
>#### Notes
>
>> - 
>
>

## OSA 1.16.5 Version ALPHA A-0.29.0

>### **Description**
>
>Added quests and updated crafting scripts
>
>#### Updated Mods
>
>>- Cloth Config API
>>- Collective
>>- CraftTweaker
>>- GeckoLib
>>- Lootr
>
>#### Removed Mods
>
>>- Simple Generators
>
>#### Added Mods
>
>-  Bluemap
>
>#### Configs
>
>-  FTBQuests
> -  generated quests
>
>
>#### Quests
>
>> - Flux Networks
>>   - added introduction quests, descriptions and rewards
>> - Botania
>>   - added introduction quests, descriptions and rewards
>>   - Added intermediate and extra quests
>> - Create
>>   - added introduction quests, descriptions and rewards
>> - Thermal Series
>>   - Added some introduction quests, descriptions and rewards
>
>#### Scripts
>
>> - fluxnetworks.zs
>>   - added terrasteel and thermal series energy cell to the recipe
>> - mininggadgets.zs
>>   - changed recipe of mining gadgets to use dragonstone instead of diamond
>> - thermalseries.zs
>>   - changed recipes of machines to include botania and create items
>>   - changed recipes of some blocks to include botania and create items
>>   - changed recipes of some items to include botania and create items
>
>#### Notes
>
>> - 

## OSA 1.16.5 Version ALPHA A-0.21.0

>### Description
>
>- First version of the 1.16.5 modpack!
>- Forge version `36.2.39`
>
>#### Updated Mods
>
>> -
>
>>#### Removed Mods
>
>>> -
>
>>#### Added Mods
>
>>> - Abnormals Core
>>> - AkashicTome
>>- Apotheosis
>>> - appleskin
>>- Applied Energistics 2
>>> - Applied Energistics Tweaker
>>> - Architecurary
>>- AttributeFix
>>> - AutoRegLib
>>- Baubley Heart Canisters
>>- BetterAdvancements
>>- Better Spawner Control
>>- Better F3 Forge
>>- Biomes O Plenty
>>- Blue Skies
>>- Bookshelf
>>- Botania
>> - Botania Additions
>> - Extra Botany
>> - Mythic Botany
>>- Botany Pots
>>- Botany Trees
>>- Bountiful
>>- Brandon's Core
>>- Building Gadgets
>>- Buzzier Bees
>>- Castle in the Sky
>>- CC: Tweaked
>> - Advanced Peripherals
>>- Ceramics
>>- Ceramic Shears
>>- Chance Cubes
>>- Charging Gadgets
>>- Chicken Chunks
>>- Chisel
>>- Chisel and Bits
>>- Chunk Pregenerator
>>- Cloth Config v4 API
>>- Clumps
>>- CodeChickenLib
>>- CoFH Core
>> - Ensorcellation
>> - Thermal Cultivation
>> - Thermal Expansion
>> - Thermal Foundation
>> - Thermal Innovation
>> - Thermal Locomotion
>>- Collective
>>- Colorful Health Bar
>>- Comforts
>>- Compact Machines 4
>>- Configured
>>- Connected Textures Mod
>>- Controllable
>>- Controlling
>>- Cooking for blockheads
>>- Corpse
>>- CraftingTweaks
>>- Crafttweaker
>>- Crash Utilities
>>- Create
>>- Create Crafts and Additions
>>- CreateTweaker
>>- CreativeCore
>>- Cucumber Library
>>- Curio of Undying
>>- Curios API
>>- Custom Starter Gear
>>- Cyclops Core
>>- Dank Storage
>>- Dark Utilities
>>- DefaultOptions
>>- Ding
>>- Draconic Evolution
>>- Drawer FPS
>>- Drawers Tooltip
>>- Elevator Mod
>>- Enchantment Descriptions
>>- EnderStorage
>>- EnderTanks
>>- Enhanced Celestials
>>- Ensorcellation
>>- Entangled
>>- Everlasting Abilities
>>- Extreme Reactors
>>- Eyes in the Darkness
>>- Farmers Delight
>>- Fast Workbench
>>- FastFurnace
>>- FastLeafDecay
>>- Featured Servers
>>- Ferrite Core
>>- Find Me
>>- Flat Bedrock
>>- Flux Networks
>>- Flywheel
>>- Forge Endertech
>>- Framed Blocks
>>- FTB Backups
>>- FTB Chunks
>>- FTB Essentials
>>- FTB Library
>>- FTB Quests
>>- FTB Ranks
>>- FTB Teams
>>- Game Menu Mod Option
>>- Game Stages
>>- Illuminations
>>- Improved Backpacks
>>- Inventory Tweaks Renewed
>>- Iron Chest
>>- Iron Shulker Boxes
>>- Item Collectors
>>- Item Filters
>>- Item Physic
>>- Item Stages
>>- JAOPCA
>>- JEI
>> - JEI Tweaker
>> - JEI Integration
>> - JEI Enchantment Info
>> - JEI Professions
>>- JourneyMap
>> - JourneyMap Integration
>>- Just another Mining Dimension
>>- Just Enough Beacons
>>- Just Enough Effect Descriptions
>>- Just Enough Resources
>>- Kiwi
>>- Large Ore Deposits
>>- Lemon Lib
>>- LibX
>>- Lootr
>>- Macaws Bridges
>> - Macaws Bridges Biomes O Plenty
>>- Mantle
>>- Materialis
>>- Mekanism
>>- Metal Barrels
>>- MineTogether
>>- Mini Utilities
>>- Mining Gadgets
>>- More Dragon Eggs
>>- More Overlays Updated
>>- More Villagers
>>- Mouse Tweaks
>>- Mystical Adaptations
>>- Mystical Agraditions
>>- Mystical Agriculture
>>- Mystical Agriculture Tiered Crystals
>>- Mystical Customization
>>- NBT Advanced Tooltips
>>- Neat
>>- Nethers Exoticism
>>- Nether Portal Fix
>>- Nethers Delight
>>- OAuth
>>- Ore Excavation
>>- Overloaded Armor Bar
>>- Pams Harvecraft 2 Crops
>> - Food Core
>> - Food Extended
>> - Fruit Trees
>>- Patchouli
>>- Performant
>>- Placebo
>>- Polymorph
>>- ProjectE
>>- Quark
>> - Quark Oddities
>>- Randompatches
>>- Ranged Pumps
>>- Recipe Stages
>>- Reqliquary
>>- Repurposed Structures
>>- Runelic
>>- Savage & Ravage
>>- Save My Stronghold
>>- Selene
>>- Serene Seasons
>> - Harvestcraft2 Compat
>>- Server Tab Info
>>- Set World Spawn Point
>>- ShetiPhian-Core
>>- Simple Generators
>>- Simple RPC
>>- SlimyBoyos
>>- Snad
>>- Snow! Real Magic!
>>- Spark
>>- Spice of Life: Carrot Edition
>>- Stoneholm
>>- Storage Drawers
>>- Storage for ComputerCraft
>>- Structure Gel API
>>- SuperMartijn642's Config Library
>>- SuperMartijn642's Core Lib
>>- Supplementaries
>>- SwingThroughGrass
>>- Tank Null
>>- The Endergetic Expansion
>>- Tinkers' Construct
>>- Tip The Scales
>>- ToolStats
>>- Trash Cans
>>- ValkyrieLib
>>- Vanilla Tweaks
>>- Waila
>> - Waila Harvestability
>> - WAWLA
>>- Wall-Jump!
>>- Waystones
>>- World Border
>>- World Stripper
>>- World Edit
>>- XP From Harvest
>>- Yungs API
>> - Better Caves
>> - Better Dungeons
>> - Better Mineshafts
>> - Bettter Strongholds
>> - Bridges
>> - Extras
>>- Zero Core 2
>
>#### Configs
>
>> - Apotheosis
>> - initialised configs
>> - disabled modules: deadly, enchantment, spawner, village
>> - Garden
>>   - set Cactus height to 8 (from 5)
>>   - set Reeds height to 12 (from 255)
>>> - appleskin
>>> - initialised config
>> - Applied Energistics 2
>>> - initialised configs
>> - increased charged certus ore spawn chance to 25% from 8%
>> - increased
>> - Baubley Heart Canister
>> - initialised config
>> - increased with bones drop rate from 15%>20%
>> - Better Advancements
>> - initialised configs
>> - order tabs alphabetically
>> - Better Caves
>> - Initialised configs
>> - changed region size to medium for caves
>> - Better Strongholds
>> - Initialised configs
>> - increased size of strongholds
>>   - grand libraries 1>3, small libraries 2>5, treasure room 2>5, portal room 1>2
>> - Biomes O Plenty
>> - initialised config
>> - set river size to large
>> - Bountiful
>> - initialised config
>> - decreased new bounty frequency to 5 minutes
>> - increased chance to spawn bounty board in a village
>> - increased coop kill distance to 12
>> - Building Gadgets
>> - initialised configs
>> - Increased paste container capacities
>> - Castle in the Sky
>> - decreased minimum distance from spawn to 1000
>> - decreased minimum and average chunk spawn attempts
>> - Charging Gadgets
>> - reduced maxEnergy to 10,000 from 1,000,000 (is meant to be an early game item, don't want it being used as a large buffer)
>> - Chicken Chunks
>> - reduced total allowed chunks per player to 75
>> - reduced chunks per loader to 50
>> - this allows 2 large loader at a a radius of 3 plus a couple of small ones, or one at 4 plus small ones
>> - Chisel and bits
>> - initialised configs
>> - disabled bits injection into JEI (was causing JEI to slow down drastically, and added many tabs)
>> - Chunk Pregenerator
>> - initialised config
>> - COFH
>> - COFH
>>   - initialised configs
>> - ensorcellation
>>   - initialised configs
>>   - disabled volley (I think there is other options, also not necessary, will consider re-enabling when pack is more finished)
>>   - disabled magic protection
>>   - disabled magic edge
>>   - enabled gourmand
>>   - enabled ender disruption (no ender IO sword, is a good replacement)
>>   - disabled leech (would prefer other mods handle this like tinkers, if it exists elsewhere, otherwise not needed)
>>   - vorpal: increased to max level 4 (5 with quark tomes), decreased crit to 4% (max of 20 with tome), head chance now max of 50% with tome
>>   - disabled pilfering (modpack is server oriented, don't want to make stealing easier, not necessary either)
>>   - disabled excavating (want to restrict that to tinkers, possibly mid-late game electric tools)
>>   - disabled furrowing, see above
>>   - disabled tilling, see above
>>   - disabled bulwark, seems unnecessary
>>   - ~~reduced phalanx max level to 1 (2 with tome)~~ Kept at level 2, quark doesn't seem to like level 1 enchantments for tomes
>>   - soulbound, reduced max level to 2 (3 with tome), disabled permanent. Increases value and reduces utility a little in favour of other potential future options
>>   - disabled trueshot
>> - Configured
>> - initialised config
>> - Corpse
>> - initialised config
>> - set corpse with loot timeout to 5 days real time (8640000 ticks)
>> - Create
>> - initialised configs
>> - enabled fireworks with more than 9 ingredients in mechanical crafter
>> - Create Crafts and Additions
>> - initialised configs
>> - DefaultOptions
>> - initialised configs
>> - Elevators
>> - Initialised config
>> - reduced elevator range to 20
>> - Ender Tanks
>> - changed size upgrades to be mana pearls for small and mana runes for large
>> - Everlasting Abilities
>> - disabled a bunch of abilities
>> - Flux Networks
>> - Initialised configs
>> - disabled chunkloading (hopefully chickenchunks can handle this seemlessly)
>> - HWYLA
>> - enabled fluid display
>> - JEI
>> - initialised config
>> - increased columns from 9-15
>> - enabled all types of searching (tooltips etc.), forced all possible to require symbols for searching
>> - JEI Integration
>> - Enabled: BurnTime, Durability tooltips
>> - Enabled on Shift & Debug: NBT, Registry Name, TranslationKey tooltips
>>   - will be leaving onShift&Debug enabled at all times since they require F3+H enabled and shift to appear, normal users won't be bothered by these
>> - Journeymap
>> - initialised config
>> - Journeymap Integration
>> - initialised config
>> - Just Enough Resources
>> - initialised configs
>> - Mantle
>> - initialised Configs
>> - Changed order of output tags, made themeral higher priority than mekanism
>> - Mekanism
>> - disabled anchor upgrade chunkloading (think the upgrade exists but does nothing)
>> - set temperature unit to `C`
>> - enabled OPs bypass security
>> - Mini Utilities
>> - added ores to the quantum quarry
>> - Mouse Tweaks
>> - initialised config
>> - Neat
>> - Initialised configs
>> - Ore Excavation
>> - Initialised configs
>> - increased hunger from excavating
>> - decreased max blocks excavated
>> - increased server tick time to mine blocks (to help balance performance)
>> - Ore Tweaker
>> - initialised configs
>> - Pams Harvestcraft Trees
>> - Increased all tree generation chances from 5000 to 2500 (lower is higher)
>> - Patchouli
>> - initialised configs
>> - Polymorph
>> - initialsed configs
>> - Quark
>> - initialised configs
>> - disabled iron rod (used by pistons for mining)
>> - disabled rope
>> - disabled back keybind (these usually interfere with my mouse sensitivity)
>> - disabled pathfinder maps (concerned about interference from other mods, also not really needed)
>> - disabled pickarang (too difficult to attain when useful, also would need to test the block levels it can break)
>> - disabled better elytra rockets (want to make elytras less useful, will have end-game alternatives)
>> - improved sleeping changed to 50% from 100%
>> - disabled crate (doesn't seem necessary, other storage methods available)
>> - disabled magnets
>> - disabled pipes (want to reserve that kind of thing for higher tech levels)
>> - Randompatches
>> - disabled remove glowing effect from potions (who doesn't want that!)
>> - added custom window titles
>> - Snad
>> - increased speed from 2>4
>> - Spice Of Life: Carrot Edition
>> - initialised configs
>> - reduced hearts gained per milestone to 1
>> - Added many milestones
>> - Storage 4 Computercraft
>> - initialised configs
>> - reduced channels used per computer to 1
>> - Storage Drawers
>> - initialised configs
>> - Tinkers' Construct
>> - slightly reduced occurances of sky slime islands
>> - massively increased occurences of sky clay islands
>> - Waila Harvestability
>> - initialised configs
>> - enabled harvest level tooltip
>> - WallJump
>> - Disabled enchantments
>> - enabled double jump
>> - Waystones
>> - initialised configs
>> - forced spawning of waystones in villages
>> - made default behaviour to spawn waystones in ALL dimensions
>> - only creative can place/edit/break
>> - only owner can rename (shouldn't be an issue anyway)
>> - enabled on journeymap
>> - Reliquary
>> - disabled gun module
>
>#### Quests
>
>> -
>
>#### Scripts
>
>> - waystones.zs
>>  - disabled all recipes
>> - buildinggadgets.zs
>> >  - changed recipes for:
>> >    - building gadget
>> >    - copy-paste gadget
>> >    - destruction gadget
>> >    - exchange gadget
>> >    - charginggadgets.zs
>> - charginggadgets.zs
>>   - changed recipe of the charging gadget
>>
>> - fluxnetworks.zs
>> >  - changed recipes:
>>    - flux core
>>    - basic flux storage
>
>#### Notes
>
>> - initial upload for curseforge verification to proceed work on pack
>
>
