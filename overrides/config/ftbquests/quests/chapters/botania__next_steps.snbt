{
	id: "6838B5611A67C62A"
	group: "39D93811111D61E8"
	order_index: 1
	filename: "botania__next_steps"
	title: "Botania - Next Steps"
	icon: {
		id: "botania:lexicon"
		Count: 1b
		tag: {
			"botania:elven_unlock": 1b
		}
	}
	default_quest_shape: ""
	default_hide_dependency_lines: false
	quests: [
		{
			x: 0.0d
			y: 0.0d
			description: [
				"Now that you've gotten familiar with the basics. It's time to step it up and begin introducing more items and mechanics"
				""
				"If you've been reading the Lexica Botania, which you should be, you'll know some flowers can interact with redstone, this is very important since it allows for vanilla minecraft to control botania. Some blocks also output signals for comparators"
			]
			dependencies: ["303AD613F9D9BF2B"]
			id: "0819719D5257B9BC"
			tasks: [{
				id: "425FBE5794076B6A"
				type: "checkmark"
				title: "Botania Next Steps"
			}]
		}
		{
			x: 1.5d
			y: 0.0d
			description: [
				"The runic altar is used to craft runes, refer to the Lexicana Botania for instructions on how to use it"
				""
				"Runes will be used widely across botania, and this whole modpack, so you'll be wanting to be familiar with this handy block"
			]
			dependencies: ["0819719D5257B9BC"]
			id: "10D74F05BA00C949"
			tasks: [{
				id: "65FAF7FB211025E9"
				type: "item"
				item: "botania:runic_altar"
			}]
			rewards: [
				{
					id: "01CEB89389811E36"
					type: "item"
					item: "minecraft:diamond"
					count: 6
				}
				{
					id: "4BF551FA7D296BA3"
					type: "item"
					item: "minecraft:lapis_lazuli"
					count: 6
				}
			]
		}
		{
			x: 3.0d
			y: 0.0d
			dependencies: ["10D74F05BA00C949"]
			id: "27E3DE4A3CB72BB4"
			tasks: [{
				id: "66963023A084EA09"
				type: "item"
				item: {
					id: "botania:lens_normal"
					Count: 1b
					tag: { }
				}
			}]
			rewards: [
				{
					id: "3A78932E5E55746E"
					type: "item"
					item: "botania:manasteel_ingot"
					count: 6
				}
				{
					id: "4405704F601C5E48"
					type: "item"
					item: "botania:mana_pearl"
					count: 6
				}
			]
		}
		{
			x: 1.5d
			y: -1.5d
			dependencies: ["10D74F05BA00C949"]
			id: "1A2AC004792B4127"
			tasks: [{
				id: "6BF216A11D3B178D"
				type: "item"
				item: "botania:rune_mana"
			}]
			rewards: [
				{
					id: "663BB492A2477EB9"
					type: "item"
					item: "botania:mana_diamond"
					count: 2
				}
				{
					id: "1D4B9C263E073868"
					type: "item"
					item: "botania:mana_powder"
					count: 8
				}
			]
		}
		{
			title: "Tree farm"
			x: 4.5d
			y: 0.0d
			description: [
				"Lenses can be applied to mana spreaders to make them do different things, lenses and slime can also be combined to make lenses with 2 effects"
				""
				"Try and make a farm which plants, harvest and feeds endoflames coal or wood. You'll need some functional flowers to achieve this, the rest is up to your imagination"
				""
				"This tree farm should act as a self sustaining mana farm, which replants its own trees and converts their logs into mana"
			]
			dependencies: ["27E3DE4A3CB72BB4"]
			id: "2C327BA257B4456A"
			tasks: [{
				id: "0B5F98BF10B07EE2"
				type: "checkmark"
				title: "Lenses can be applied to mana spreaders by right clicking. Different lenses have different effects, why don't you try and setup a tree farm which plants, harvests and feeds endoflames coal or wood"
			}]
		}
		{
			x: 6.0d
			y: 0.0d
			description: ["Sparks can transfer mana around much faster than Mana Spreaders, but only to certain blocks/structures. Keep an eye on your Lexicana Botania to see what they are used for"]
			dependencies: ["2C327BA257B4456A"]
			id: "58A65CD918630EB2"
			tasks: [{
				id: "578505C1E31EF2F0"
				type: "item"
				item: "botania:spark"
			}]
			rewards: [{
				id: "073E921E95F62EDF"
				type: "xp_levels"
				xp_levels: 5
			}]
		}
		{
			x: 7.5d
			y: 0.0d
			description: [
				"This wonderful block, as part of a structure described in your Lexicana Botania will help you access terrasteel, which itself starts the late-game botania progress."
				""
				"Warning, crafting terrasteel will require sparks, and half a pool of mana"
			]
			dependencies: ["58A65CD918630EB2"]
			id: "31AE73AB69DC9202"
			tasks: [{
				id: "51731AFAE169FECD"
				type: "item"
				item: "botania:terra_plate"
			}]
			rewards: [
				{
					id: "036277D2FA8B8151"
					type: "item"
					item: "botania:mana_diamond"
					count: 3
				}
				{
					id: "0BE18F502535F11C"
					type: "item"
					item: "botania:mana_pearl"
					count: 6
				}
				{
					id: "33D12535E3C82E42"
					type: "item"
					item: "botania:manasteel_ingot"
					count: 12
				}
			]
		}
		{
			x: 9.0d
			y: 0.0d
			dependencies: ["31AE73AB69DC9202"]
			id: "4AC80DDD41900092"
			tasks: [{
				id: "164D2C0C43DB3EE2"
				type: "item"
				item: "botania:terrasteel_ingot"
			}]
			rewards: [{
				id: "31D114697FA1130B"
				type: "item"
				item: "botania:terrasteel_ingot"
				count: 4
			}]
		}
		{
			x: 12.0d
			y: 0.0d
			description: [
				"The portal to Alfeihm is a structure you can build to exchange items for elven versions, the Lexicana Botania itself can be exchanged for a better version aswell"
				""
				"Make sure to refer to your Lexicana Botania for how to craft the portal, you'll be needing lots of mana and a couple of sparks to open it"
			]
			dependencies: ["2A9CF3F9E6D0E3C8"]
			id: "51048840413D7683"
			tasks: [{
				id: "5B936A2C9B374586"
				type: "checkmark"
				title: "Alfeihm Portal"
			}]
			rewards: [{
				id: "73903A8F766DACD8"
				type: "xp_levels"
				xp_levels: 1
			}]
		}
		{
			x: 10.5d
			y: 0.0d
			description: ["The next goal is making a portal to Alfheim, make sure to refer to your Lexicana Botania on how to do so"]
			dependencies: ["4AC80DDD41900092"]
			id: "2A9CF3F9E6D0E3C8"
			tasks: [{
				id: "656BCA67FA89E7FD"
				type: "item"
				item: "botania:alfheim_portal"
			}]
			rewards: [{
				id: "4236C3371371B919"
				type: "item"
				item: "botania:terrasteel_ingot"
			}]
		}
		{
			x: 13.0d
			y: 1.0d
			description: [
				"This flower becomes available once you've used the placed the Lexica Botania in the Alfheim portal"
				""
				"The Rosa Arcana can make mana from experience, simply stand close and the flower will absorb your experience!"
			]
			dependencies: ["51048840413D7683"]
			id: "114B4F4CAA2E7D7B"
			tasks: [{
				id: "170CE0B5400C777A"
				type: "item"
				item: "botania:rosa_arcana"
			}]
			rewards: [{
				id: "1F5FD565C2668FC7"
				type: "item"
				item: "botania:rosa_arcana"
				count: 3
			}]
		}
		{
			x: 12.5d
			y: 2.0d
			description: ["The Kekimuru can eat cakes and make mana from them! Isn't that incredible, why don't you try and automate the crafting"]
			dependencies: ["51048840413D7683"]
			id: "79777B181469D9CD"
			tasks: [{
				id: "5957A4D57C13A0AE"
				type: "item"
				item: "botania:kekimurus"
			}]
			rewards: [{
				id: "019441A6EBD74B1A"
				type: "item"
				item: "botania:kekimurus"
				count: 2
			}]
		}
		{
			x: 11.0d
			y: 1.0d
			description: [
				"The hopperhock can pickup nearby items and place them into chests, like a hopper but wireless."
				""
				"Providing the hopperhock with mana will increase the range it can pickup items, you can also filter items which g into chests"
				""
				"Make sure to read up about the flower in the Lexica Botania, this one is very handy for automating setups"
			]
			dependencies: ["51048840413D7683"]
			id: "7E6BA8F361C50A88"
			tasks: [{
				id: "6F99E7FBB4248B74"
				type: "item"
				item: "botania:hopperhock"
			}]
			rewards: [{
				id: "19148481BA05B7A8"
				type: "item"
				item: "botania:hopperhock"
				count: 3
			}]
		}
		{
			x: 11.5d
			y: 2.0d
			description: ["The Orechid can convert stone into ores! There's also a nether version"]
			dependencies: ["51048840413D7683"]
			id: "053C7041F6971C58"
			tasks: [{
				id: "4B4F42E1FA6FC6EA"
				type: "item"
				item: "botania:orechid"
			}]
			rewards: [
				{
					id: "2771625673A5696A"
					type: "item"
					item: "botania:orechid"
				}
				{
					id: "441E934D82C77B0E"
					type: "item"
					item: "botania:orechid_ignem"
				}
			]
		}
		{
			x: 13.5d
			y: 0.0d
			description: ["For those more technically inclined, Botania can convert Mana into FE, great for your electric machines!"]
			dependencies: ["51048840413D7683"]
			id: "6348A7F4BB0BDA17"
			tasks: [{
				id: "4CB1DF436FBB2646"
				type: "item"
				item: "botania:mana_fluxfield"
			}]
			rewards: [
				{
					id: "0AD5A838DDCCE0BC"
					type: "xp_levels"
					xp_levels: 5
				}
				{
					id: "79BE43D26746E361"
					type: "item"
					item: "thermal:energy_cell_frame"
				}
			]
		}
	]
}
