{
	id: "188CF62F3B74E3E6"
	group: "41922ED5D05A1C49"
	order_index: 0
	filename: "cooking__getting_started"
	title: "Cooking - Getting Started"
	icon: "cookingforblockheads:white_kitchen_floor"
	default_quest_shape: ""
	default_hide_dependency_lines: false
	quests: [
		{
			x: 0.0d
			y: 0.0d
			description: [
				"It is well worth to invest time into cooking in this pack, cooking can gain you plenty of extra health"
				""
				"When I say plenty, I mean bars, not hearts. Bars of extra health with an S"
				""
				"To get started, you should build a kitchen, this will simplify making many of the complicated recipes"
			]
			id: "5FAB1EBEF1E1798A"
			tasks: [{
				id: "28D35125B8346510"
				type: "checkmark"
				title: "Cooking - Getting Started"
			}]
			rewards: [{
				id: "080FE797DFFE3759"
				type: "item"
				item: "cookingforblockheads:no_filter_edition"
			}]
		}
		{
			x: 2.0d
			y: 0.0d
			description: ["Many of the \"Cooking for Blockheads\" blocks are multiblocks, so other blocks connected to them can communicate, this allows you to build out large structures that you can cook from using storage elsewhere in the room etc."]
			dependencies: ["5FAB1EBEF1E1798A"]
			id: "71878744327ED932"
			tasks: [{
				id: "5F52676CA4A420A2"
				type: "item"
				item: "cookingforblockheads:white_kitchen_floor"
			}]
			rewards: [
				{
					id: "1F3CCE3B6ACEF8E2"
					type: "item"
					item: "cookingforblockheads:white_kitchen_floor"
					count: 64
				}
				{
					id: "6F4F5129C2CBB3E3"
					type: "item"
					item: "cookingforblockheads:white_kitchen_floor"
					count: 64
				}
			]
		}
		{
			x: 4.0d
			y: 0.0d
			description: ["This is the heart of the kitchen, you can see all your available recipes here, double clicking one crafts it using ingredients and tools from around your kitchen"]
			dependencies: ["71878744327ED932"]
			id: "52679FAE532D553C"
			tasks: [{
				id: "0B04EAC8E4F11EE7"
				type: "item"
				item: "cookingforblockheads:cooking_table"
			}]
			rewards: [{
				id: "05753C3D1646D53F"
				type: "item"
				item: "cookingforblockheads:counter"
				count: 4
			}]
		}
		{
			x: 6.0d
			y: 0.0d
			description: [
				"The Oven is your second-heart of the kitchen, used for cooking things and also for utilizing pots, pans and other tools for recipes"
				""
				"Use the cow spawn egg to make a \"Cow in a Jar\" It can provide infinite milk for recipes!"
				""
				"To make the Cow In A Jar, drop an anvil onto a cow that is above a Milk Jar from the Cooking For Blockheads mod"
			]
			dependencies: ["52679FAE532D553C"]
			id: "5D6AF4B3E8C5D079"
			tasks: [{
				id: "07A701C73750A229"
				type: "item"
				item: "cookingforblockheads:oven"
			}]
			rewards: [
				{
					id: "1789485C220AC14D"
					type: "item"
					item: "cookingforblockheads:spice_rack"
					count: 2
				}
				{
					id: "67A4040A1D4A16A7"
					type: "item"
					item: "cookingforblockheads:fruit_basket"
					count: 2
				}
				{
					id: "2F8B59F36262C931"
					type: "item"
					item: "minecraft:cow_spawn_egg"
				}
				{
					id: "10D382956DBCA7C5"
					type: "item"
					item: "cookingforblockheads:milk_jar"
				}
			]
		}
		{
			x: 6.0d
			y: -2.0d
			description: ["Right-Click this onto an oven to allow it to cook with FE, this will save you having to have fuel"]
			dependencies: ["5D6AF4B3E8C5D079"]
			id: "4775FB78D349104A"
			tasks: [{
				id: "0981A0CBEFEB6944"
				type: "item"
				item: "cookingforblockheads:heating_unit"
			}]
		}
		{
			x: 8.0d
			y: 0.0d
			description: [
				"The fridge, like the counter can store food. Though the fridge can be upgraded like the oven"
				""
				"When upgraded the fridge can preserve the last ingredient and/or provide snow/ice to recipes"
			]
			dependencies: ["5D6AF4B3E8C5D079"]
			id: "1F61ECF337C5650D"
			tasks: [{
				id: "3123CA193F12589C"
				type: "item"
				item: "cookingforblockheads:fridge"
			}]
			rewards: [
				{
					id: "2660AD780E95ABAC"
					type: "item"
					item: "cookingforblockheads:tool_rack"
					count: 2
				}
				{
					id: "6319C605A198116F"
					type: "item"
					item: "cookingforblockheads:toaster"
				}
			]
		}
		{
			x: 8.0d
			y: -2.0d
			description: ["Right-Clicking this onto a Fridge allows the Fridge to provide snow and ice for recipes even without any in the fridge!"]
			dependencies: ["1F61ECF337C5650D"]
			id: "67959F6321546158"
			tasks: [{
				id: "15E06ACB52B07DCC"
				type: "item"
				item: "cookingforblockheads:ice_unit"
			}]
			rewards: [{
				id: "6604DDE188DE13CA"
				type: "item"
				item: "cookingforblockheads:preservation_chamber"
			}]
		}
		{
			x: 10.0d
			y: 0.0d
			description: [
				"Some recipes need water, the sink can provide infinite water"
				""
				"Now you should be ready to cook just about anything, you just might need some pots, pans, knives and more tools placed into your kitchen equipment"
				""
				"Make sure everything is connected, if not directly, than indirectly through kitchen floors"
			]
			dependencies: ["1F61ECF337C5650D"]
			id: "2E2F113E75BA40BE"
			tasks: [{
				id: "745F36D5FA22179E"
				type: "item"
				item: "cookingforblockheads:sink"
			}]
			rewards: [{
				id: "0E7CF45F5E48C00E"
				type: "xp_levels"
				xp_levels: 15
			}]
		}
	]
}
