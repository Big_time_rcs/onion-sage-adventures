// change philosophers stone recipe
craftingTable.removeByName("projecte:philosophers_stone");
craftingTable.removeByName("projecte:philosophers_stone_alt");
craftingTable.addShaped("projecte_philosophers_stone",
  <item:projecte:philosophers_stone>,
  [
    [<tag:items:forge:dusts/mana>, <tag:items:forge:nuggets/alfsteel>, <tag:items:forge:dusts/mana>],
    [<tag:items:forge:nuggets/alfsteel>, <item:draconicevolution:awakened_core>, <tag:items:forge:nuggets/alfsteel>],
    [<tag:items:forge:dusts/mana>, <tag:items:forge:nuggets/alfsteel>, <tag:items:forge:dusts/mana>]
  ]
);

// change recipe of energy condenser
craftingTable.removeByName("projecte:condenser_mk1");
craftingTable.addShaped("projecte_condenser_mk1",
  <item:projecte:condenser_mk1>,
  [
    [<tag:items:forge:obsidian>, <item:projecte:philosophers_stone>.transformReplace(<item:projecte:philosophers_stone>), <tag:items:forge:obsidian>],
    [<tag:items:forge:gems/diamond>, <item:projecte:alchemical_chest>, <tag:items:forge:gems/diamond>],
    [<tag:items:forge:obsidian>, <tag:items:forge:gems/diamond>, <tag:items:forge:obsidian>]
  ]
);