// restrict wings potions to specific game stage
import mods.itemstages.ItemStages;

ItemStages.restrict(<tag:items:osa:wings_stage>, "flight_wings");