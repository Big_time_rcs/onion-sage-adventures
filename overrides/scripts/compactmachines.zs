// change recipe for machine wall
craftingTable.removeByName("compactmachines:wall");
craftingTable.addShaped("compactmachines_wall",
  <item:compactmachines:wall> * 16,
  [
    [<tag:items:forge:dusts/redstone>],
    [<tag:items:forge:storage_blocks/manasteel>]
  ]
);

// change recipe for personal shrinking device
craftingTable.removeByName("compactmachines:personal_shrinking_device");
craftingTable.addShaped("compactmachines_personal_shrinking_device",
  <item:compactmachines:personal_shrinking_device>,
  [
    [<item:minecraft:air>, <tag:items:forge:glass_panes>, <item:minecraft:air>],
    [<item:botania:mana_pearl>, <item:draconicevolution:wyvern_core>, <item:botania:mana_pearl>],
    [<item:minecraft:air>, <item:createaddition:tesla_coil>, <item:minecraft:air>]
  ]
);