

// <recipetype:create:splashing>.addRecipe(String name, MCWeightedItemStack[] output, IIngredient input, @Optional(100) int duration)
// add recipe for uranium crushed to yellorium ingot
<recipetype:create:splashing>.addRecipe("splashing_crushed_uranium",
  [<item:bigreactors:yellorium_ingot>, <item:bigreactors:yellorium_ingot> % 40],
  <item:create:crushed_uranium_ore>
);