// remove direct craft recipes for hardened caseings
craftingTable.removeByName("bigreactors:reactor/reinforced/casing_alt");
craftingTable.removeByName("bigreactors:turbine/reinforced/casing_alt");

// change recipe for reactor housing
craftingTable.removeByName("bigreactors:reactor/basic/casing");
craftingTable.addShaped("bigreactors_reactor/basic/casing",
  <item:bigreactors:basic_reactorcasing>,
  [
    [<tag:items:forge:ingots/bronze>, <tag:items:forge:ingots/graphite>, <tag:items:forge:ingots/bronze>],
    [<tag:items:forge:ingots/graphite>, <tag:items:forge:sand>, <tag:items:forge:ingots/graphite>],
    [<tag:items:forge:ingots/bronze>, <tag:items:forge:ingots/graphite>, <tag:items:forge:ingots/bronze>]
  ]
);

// change recipe for turbine housing
craftingTable.removeByName("bigreactors:turbine/basic/casing");
craftingTable.addShaped("bigreactors_turbine/basic/casing",
  <item:bigreactors:basic_turbinecasing>,
  [
    [<tag:items:forge:ingots/bronze>, <tag:items:forge:ingots/cyanite>, <tag:items:forge:ingots/bronze>],
    [<tag:items:forge:ingots/cyanite>, <tag:items:forge:storage_blocks/redstone>, <tag:items:forge:ingots/cyanite>],
    [<tag:items:forge:ingots/bronze>, <tag:items:forge:ingots/cyanite>, <tag:items:forge:ingots/bronze>]
  ]
);