#loader crafttweaker

#priority 1000

import crafttweaker.api.tag.MCTag;

// add items to andesite_recipe tag
<tag:items:osa:andesite_recipe>.add(
  [
    <item:chisel:limestone/raw>,
    <item:chisel:marble/raw>,
    <item:quark:marble>,
    <item:create:gabbro_cobblestone>,
    <item:create:limestone_cobblestone>,
    <item:create:dolomite_cobblestone>,
    <item:create:scoria>,
    <item:quark:cobbled_deepslate>
  ]
);

// add items to osa_wings_stage tag
<tag:items:osa:wings_stage>.add(
  [
    <item:wings:angel_wings_bottle>,
    <item:wings:parrot_wings_bottle>,
    <item:wings:slime_wings_bottle>,
    <item:wings:blue_butterfly_wings_bottle>,
    <item:wings:monarch_butterfly_wings_bottle>,
    <item:wings:fire_wings_bottle>,
    <item:wings:bat_wings_bottle>,
    <item:wings:fairy_wings_bottle>,
    <item:wings:evil_wings_bottle>,
    <item:wings:dragon_wings_bottle>
  ]
);

// add logs/planks/sticks to tags
<tag:items:minecraft:planks>.add(
  [
    <item:feywild:spring_planks>,
    <item:feywild:summer_planks>,
    <item:feywild:autumn_planks>,
    <item:feywild:winter_planks>
  ]
);
<tag:blocks:minecraft:planks>.add(
  [
    <block:feywild:spring_planks>,
    <block:feywild:summer_planks>,
    <block:feywild:autumn_planks>,
    <block:feywild:winter_planks>
  ]
);

// add stones to stone tag
<tag:items:forge:stone>.add(
  [
    <item:chisel:marble/raw>,
    <item:chisel:limestone/raw>
  ]
);