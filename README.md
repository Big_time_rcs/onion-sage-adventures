## Script Overview

This repository contains two scripts:

* `_Archive-Repository.ps1`
* `_Extract-TwitchExport.ps1`

These are used to aid importing the repository into the Twitch client, and to export changes made in the mod-pack back
into the repository.

### Importing into Twitch

To prepare the repository for importing into Twitch, run the `_Archive-Repository.ps1` script. For example (the
`OutFile` parameter is optional):

```powershell
.\_Archive-Repository.ps1 -OutFile 'onion-sage-adventures'
```



If no `-OutFile` is specified you will be asked for a `Version` (the `Version` parameter is optional):
```powershell
Version:
```
You may input a version name here which will be added to the default filename:
```powershell
Version: B-0.3.2
```
Would result in: `Onion Sage Adventures-B-0.3.2.zip`



If no `Verion` is specified, the resulting ZIP archive will be named `Onion Sage Adventures-gitlab-export` for ease of recognition when importing to twitch.



The resulting ZIP archive can then be imported into Twitch as-is.



If the archive contains an appropriately named `Version` and correct `Manifest.json`/`modlist.html`'s, the ZIP archive may be directly uploaded to curse

### Exporting from Twitch

To export changes made to the mod-pack back into the repository, run the `_Extract-TwitchExport.ps1` script. For
example:

```powershell
.\_Extract-TwitchExport.ps1 -Path 'D:\git\Onion Sage Adventures.zip'
```

The changes are then ready to be committed to version control.

## Uploading to CurseForge

To upload the repository to CurseForge as a mod-pack:

If no recent exported Twitch archive:

1. Clone `master`.
2. Run `.\_Archive-Repository.ps1`.
3. Import the ZIP archive to Twitch (to check that it should work).
4. Upload the ZIP archive to CurseForge.

If recently imported a **working** Twitch archive:

1. Check manifest has intended `Version` parameter
2. Run `.\_Archive-Repository.ps1`.
3. Don't specify `-OutFile` parameter
4. Specify appropriate *prompted* `Version` parameter
5. Upload to CurseForge

*Note, this method is for changes that you are sure are working as-is and don't require a new `manifest` or `modlist.html`*